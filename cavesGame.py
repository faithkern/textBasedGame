# dungeon monster game
import sys
import random

exp = 0 #10exp = new level
level = 1
floor = 1 #goes up to 25(surface)
#damage based on floors (gets harder as floor increases)

class Monster: #has name, type, hp, att, & less damage taken(shield)
    def __init__(self, n, num):
        self.name = n
        self.less = 0
        self.attbonus = 0
        if num==1:
            self.type = "Beast"
            self.hp = 11
            self.attack = 3
        elif num==2:
            self.type = "Demon"
            self.hp = 9
            self.attack = 5
        elif num==3:
            self.type = "Dragon"
            self.hp = 10
            self.attack = 4
        elif num==4:
            self.type = "Vampire"
            self.hp = 12
            self.attack = 2
        self.realHP = self.hp #to restore after fights
    def hurt(self):
        if floor < 8:
            rad = random.randint(1,6)
        elif floor < 20:
            rad = random.randint(1,10)
        elif floor < 25:
            rad = random.randint(1,16)
        elif floor == 25: #king
            rad = random.randint(5,20)
        self.hp = self.hp - rad - self.less
    def restore(self):
        self.hp = self.realHP
    def levelUp(self):
        self.hp = self.hp + 1
        self.realHP = self.realHP + 1
        self.attack = self.attack + 1
    def bonusAttack(self,bonus):
        self.attbonus = bonus
    def lessDamage(self,less):
        self.less = less
        
    def getHP(self):
        return self.hp
    def getName(self):
        return self.name
    def getType(self):
        return self.type
    def getDef(self):
        return self.less
    def totalAttack(self):
        a = self.attack + self.attbonus
        return a
    def getBA(self):
        return self.attbonus
    def getAtt(self):
        return self.attack

class Enemy: #has type, hp = level
    def __init__(self, num):
        if num==1:
            self.type = "Knight"
        elif num==2:
            self.type = "Wizard"
        elif num==3:
            self.type = "Elf"
        elif num==4:
            self.type = "Villager"
        self.hp = level
    def hurt(self,M):
        rad = random.randint(1,M.totalAttack())
        self.hp = self.hp - rad
    def getName(self):
        return self.type
    def gethp(self):
        return self.hp

class King: #has type, hp = 50
    def __init__(self):
        self.type = "King"
        self.hp = 50
    def hurt(self,M):
        rad = random.randint(5,M.totalAttack())
        self.hp = self.hp - rad
    def getHP(self):
        return self.hp
    def getName(self):
        return self.type

class Weapon: #has type, +attack
    def __init__(self, num):
        if num==1:
            self.type = "Knife"
            self.att = 1
        elif num==2:
            self.type = "Jagged Dagger"
            self.att = 2
        elif num==3:
            self.type = "Bow and Arrow"
            self.att = 3
        elif num==4:
            self.type = "Hammer"
            self.att = 4
        elif num==5:
            self.type = "Broadsword"
            self.att = 5
        elif num==6:
            self.type = "Poisoned Sword"
            self.att = 6
    def getAtt(self):
        return self.att
    def getName(self):
        return self.type

class Shield: #has type, +shield
    def __init__(self, num):
        if num==1:
            self.type = "Leather Bracers"
            self.de = 1
        elif num==2:
            self.type = "Metal Bracers"
            self.de = 2
        elif num==3:
            self.type = "Small Shield"
            self.de = 3
        elif num==4:
            self.type = "Wooden Shield"
            self.de = 4
        elif num==5:
            self.type = "Large Shield"
            self.de = 5
        elif num==6:
            self.type = "Metal Shield"
            self.de = 6
    def getDef(self):
        return self.de
    def getName(self):
        return self.type

def levelUp(self,M):
    level = level + 1
    exp = 0
    M.levelUP()

class Room:
    def __init__(self, num):
        if num < 50:
            self.room = "Empty"
        elif num < 68:
            self.room = "Weapon"
        elif num < 75:
            self.room = "Shield"
        elif num < 85:
            self.room = "Enemy"
        elif num <= 100:
            self.room = "Stairs"
    def getType(self):
        return self.room

def statistics(M):
    one = str(M.getName())
    two = str(M.getType())
    three = str(M.getHP())
    four = str(M.getAtt())
    five = str(M.getBA())
    six = str(M.getDef())
    seven = str(level)
    eight = str(exp)
    nine = str(floor)
    print("\nName: "+one+"\nMonster: "+two+"\nHP: "+three)
    print("Attack: "+four+"\nWeapon Attack Bonus: "+five+"\nShield Defense Bonus: "+six)
    print("Current Level: "+seven+"\nCurrent EXP: "+eight+"/10\nCurrent Floor: "+nine+"\n")

yes = ["yes","yeah","y","ya"]
no = ["no","nope","n","nah"]
lrs = ["left","right","straight","l","r","s"]
attack = ["a","att","attack","fight"]
flee = ["flee","f"]

########################################################################

#stories at certain floors:

def Story(M):
    if floor == 7:
        print("\nYou have traveled through 7 floors of your home, fighting "
        +"the \nKing's minions along the way.\n\nIt has been a stressful time,"
        +" finding the corpses of monsters and \nbeasts that you used to know."
        +"\n\nYou have to keep moving, for the good of your home, and your family.\n")
        print("-------------------- "+M.getName()+"\'s JOURNEY CONTINUES --------------------")
    elif floor == 13:
        print("\nYou had just slain a despicable human, when you heard yelling"
        +" from your left.\n\n\'"+str(M.getName())+"!!\' You turned to see "
        +"a baby dragon flying and \nstumbling towards you.\n\nYou knew that "
        +"dragon! She lived down where you did. \nHer mother was your friend."
        +"\n\nShe looks very upset, and you ask her what's wrong. \'My mother...\'"
        +"\n\nHer mother? Gone? No, it couldn't be. She was so strong..."
        +"\n\nYou told the baby dragon that you would avenge her mother,"
        +" and that you would\ncome back for her. You promised.\n")
        print("-------------------- "+M.getName()+"\'s JOURNEY CONTINUES --------------------")
    elif floor == 20:
        print("\nYou don't even know what floor you're on at this point."
        +"\n\nYou think you must be close. It's been so long, but you've gotten"
        +"\nso much stronger since you were in hiding down on the bottom floor."
        +"\n\nYou're almost there.\n")
        print("-------------------- "+M.getName()+"\'s JOURNEY CONTINUES --------------------")

def SurfaceStory():
    print("\nYou climbed the last flight of stairs, the sun - unfamiliar\nto you -"
    +" shining in your face. And there he stood. The King.\n\n"
    +"You thought about all your friends, the Goblin, the baby \ndragon and her"
    +" mother.\n\nYou were ready to fight for them.\n")

def EndStory():
    print("\nFinally, the King was dead and the humans were fleeing the area."
    +"\n\nYou looked up at the sky for the first time in so long, enjoying the"
    +" fresh air \nand twinkling stars overhead.\n\nIt was a good night."
    +"\n\nYou retreated back into the Caves, only to gather every monster \nand"
    +" dragon and demon and vampire you could find to bring them \nto the surface. They"
    +" deserved to see this world, too. \n\nEveryone thanked you gratefully,"
    +" not knowing how to repay you. You assured them \nthat they didn't need to"
    +" do anything at all. After all, you wanted \nto be free, too."
    +"\n\nYou were only truly happy once you saw that baby dragon come up out"
    +" \nof the Caves. You took her in and promised to raise her on the "
    +"\nsurface world.\n\nEveryone who survived the King's wrath lived freely"
    +" above ground with you \nas their leader, and they knew that with you "
    +"around they would be safe \nfor a long, long time.\n")
    print("-------------------- THE END --------------------")


########################################################################

print("You awoke one morning to a horrible sound. \n"
      +"In the dark caves you live in underground, "+
      "you could easily see \nyour friend Goblin running to you.\n")
while True:
    name = input("Enter your Hero's name: ")
    if name == "":
        print("\nYou must enter a valid name.")
        continue
    else:
        break
print("\n\'"+name+"! Wake up! Something has happened above us. \n"+
      "The King of the country above ground has sent his minions \n"+
      "to come slaughter us all! You are the strongest of us. \n"+
      "Please, you must stop them!\'\n")
while True:
    print("Will you help your friends and family and fight your way to the King?"
      +"\n\t- Y(es)\n\t- N(o)")
    answer = input("> ")
    if answer.lower() in no:
        print("\nGame Over. The King has killed you all.")
        sys.exit()
    elif answer.lower() in yes:
        print("\nYou have taken it upon yourself to protect your home from the "
              +"evil humans \n"+
              "and their King. You are on the bottom floor of the Caves in "
              + "which you dwell. \n"+
              "Make it to the Surface, Floor 25, and defeat the King to "
              + "save your home! \n")
        break
    else:
        print("\nYou must make a valid decision.")
        continue
print("Before you start your journey, here are your base stats: \n")

ran = random.randint(1,4)
M = Monster(name,ran)

print("You are a "
      +str(M.getType())+
      " named "
      +str(M.getName())+
      ". You have "
      +str(M.getHP())+
      " HP and "
      +str(M.totalAttack())+
      " Attack Power. \nYou currently have no shield or weapon and you are at Level 1.\n\n"
      +"Type \'STATS\' at any time to see your stats.\n")
print("Your Goblin friend taps your leg. \'Please hurry! We're all depending "
      +"on you!\' \n")
print("-------------------- "+str(M.getName())+"\'s JOURNEY BEGINS --------------------")

while floor<25:
    ran = random.randint(0,100)
    R = Room(ran)
    if R.getType()=="Empty":
        while True:
            print("\nThis room is empty. Choose a direction to travel: "+
            "\n\t- L(eft) \n\t- R(ight) \n\t- S(traight)")
            answer = input("> ")
            if answer.lower() in lrs:
                break
            elif answer == "STATS":
                statistics(M)
                continue
            else:
                print("\nYou must choose a valid direction.")
                continue
    elif R.getType()=="Stairs":
        while True:
            print("\nYou found a staircase! Travel to the next floor?"+
            "\n\t- Y(es) \n\t- N(o)")
            answer = input("> ")
            if answer.lower() in yes:
                floor = floor + 1
                print("You are now on floor "+str(floor)+".")
                level = level + 1
                M.levelUp()
                print("Level up! Your current level is now "+str(level)+".")
                Story(M) #when floor ++
                break
            elif answer.lower() in no:
                break
            elif answer == "STATS":
                statistics(M)
                continue
            else:
                print("\nYou must make a valid decision.")
                continue
    elif R.getType()=="Weapon":
        r = random.randint(1,6)
        W = Weapon(r)
        while True:
            print("\nYou found a weapon! It is a "+str(W.getName())+
            " with "+str(W.getAtt())+" Attack \nBonus. Your current Attack "
                  +"Bonus is "+str(M.getBA())+". Equip new weapon? \n"
            "(Weapons that are not equipped will gain you EXP.)"+
            "\n\t- Y(es) \n\t- N(o)")
            answer = input("> ")
            if answer.lower() in yes:
                M.bonusAttack(W.getAtt()) #set new att bonus
                break
            elif answer.lower() in no:
                exp = exp + 2
                if exp>=10:
                    exp = exp - 10
                    level = level + 1
                    M.levelUp()
                    print("Level up! Your current level is now "+str(level)+".")
                break
            elif answer == "STATS":
                statistics(M)
                continue
            else:
                print("\nYou must make a valid decision.")
                continue
    elif R.getType()=="Shield":
        r = random.randint(1,6)
        S = Shield(r)
        while True:
            print("\nYou found a shield! It is a "+str(S.getName())+
            " with "+str(S.getDef())+" Defense \nBonus. Your current Defense "
                  +"Bonus is "+str(M.getDef())+". Equip new shield? \n"
            "(Shields that are not equipped will gain you EXP.)"+
            "\n\t- Y(es) \n\t- N(o)")
            answer = input("> ")
            if answer.lower() in yes:
                M.lessDamage(S.getDef()) #set new shield bonus
                break
            elif answer.lower() in no:
                exp = exp + 2
                if exp>=10:
                    exp = exp - 10
                    level = level + 1
                    M.levelUp()
                    print("Level up! Your current level is now "+str(level)+".")
                break
            elif answer == "STATS":
                statistics(M)
                continue
            else:
                print("\nYou must make a valid decision.")
                continue
    elif R.getType()=="Enemy":
        r = random.randint(1,4)
        E = Enemy(r)
        print("\nThere's an Enemy! It's a "+str(E.getName())+".")
        while True:
            print("\t- A(ttack) \n\t- F(lee)")
            answer = input("> ")
            if answer.lower() in attack:
                E.hurt(M)
                if(E.gethp()<=0):
                    print("\nYou defeated the "+str(E.getName())+"! +4 EXP. HP restored.")
                    M.restore()
                    exp = exp + 4
                    if exp>=10:
                        exp = exp - 10
                        level = level + 1
                        M.levelUp()
                        print("Level up! Your current level is now "+str(level)+".")
                    break
                else:
                    M.hurt()
                    print("\nYou hit the "+str(E.getName())+". It hit you back. Your current"
                    +" HP is "+str(M.getHP())+".\nIt's HP is "+str(E.gethp())+"." )
                    if M.getHP()<=0:
                        print("\nSadly, this is the end of "+M.getName()+"\'s journey. You were"
                        +" slain by a " + str(E.getName())+". \n(Type "
                        +" \'STATS\' ot view your stats, or hit the \'ENTER\' "
                        +"key to exit.)")
                        answer = input("> ")
                        if answer == "STATS":
                            statistics(M)
                            sys.exit()
                        sys.exit()
                    continue
            elif answer.lower() in flee:
                print("You ran away, Scaredy-Cat. No EXP for you. (But sure, "
                +"\nHP restored... I guess...)")
                M.restore()
                break
            elif answer == "STATS":
                statistics(M)
                continue
            else:
                print("\nYou must decide what to do!")
                continue

if floor == 25: #boss fight
    SurfaceStory()
    K = King()
    while True:
        print("\nDefeat the King at all costs.\n\t- A(ttack)")
        answer = input("> ")
        if answer.lower() in attack:
            K.hurt(M)
            if(K.getHP()<=0):
                    print("\nYou have defeated the King!")
                    M.restore()
                    EndStory()
                    print("\nYou can type \'STATS\' to view your stats"
                    +"\nfor this game.")
                    break
            else:
                    M.hurt()
                    print("\nYou hit the King. He hit you back. Your current"
                    +" HP is "+str(M.getHP())+".\nHis HP is "+str(K.getHP()))
                    if M.getHP()<=0:
                        print("\nSadly, this is the end of "+M.getName()+"\'s journey. You were"
                        +" slain by the King. \n(Type "
                        +" \'STATS\' ot view your stats, or hit the \'ENTER\' "
                        +"key to exit.)")
                        answer = input("> ")
                        if answer == "STATS":
                            statistics(M)
                            sys.exit()
                        sys.exit()
                    continue
        else:
            print("\nYou must attack.")
